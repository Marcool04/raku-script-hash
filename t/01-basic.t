#!/usr/bin/env raku

use Test;
use Script::Hash;

ok $script-hash ~~ Str:D, "\$script-hash is a defined string";

my $prog_string = 'use Script::Hash; say $script-hash';
my $hash1 = run("raku", "-e", "$prog_string", "arg1", "arg2", :out).out.lines[0];
my $hash2 = run("raku", "-e", "$prog_string", "arg3", "arg4", :out).out.lines[0];
ok $hash1 ne $hash2, "change of arguments changes hash";

my $test_prog1 = "/tmp/test_prog1.raku";
$test_prog1.IO.spurt($prog_string);
my $hash3 = run("raku", "$test_prog1", :out).out.lines[0];
my $test_prog2 = "/tmp/test_prog2.raku";
$test_prog2.IO.spurt($prog_string);
my $hash4 = run("raku", "$test_prog2", :out).out.lines[0];
ok $hash3 ne $hash4, "change of program name changes hash";
unlink $test_prog1;
unlink $test_prog2;

done-testing;
