#!/usr/bin/env raku

use Test;

# Will find and raku -c all files with these extensions:
my @extensions = ("raku", "rakumod");

my @files_to_check;
my @todo = $*CWD.IO;
while @todo {
  for @todo.pop.dir -> $path {
    if ($path.d) {
      @todo.push: $path;
    }
    if ( $path.f && ($path.extension ∈ @extensions) ) {
      @files_to_check.push: $path;
    }
  }
}
for @files_to_check -> $file {
  is run("raku", "-c", "$file").exitcode, 0, "Syntax of $file is OK";
}
done-testing;
