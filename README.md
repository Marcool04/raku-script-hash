# NAME
Script::Hash - A module to export a md5 hash of program name and arguments

# SYNOPSIS
```raku
use Script::Hash;
say "$script-hash" # output, example: 5067EEFF4999C7B5B4F593DD3521B9
```

# DESCRIPTION
`Script::Hash` exports a single varibale, `$script-hash`, that can be used to access an md5 hash of the program name (`$*PROGRAM-NAME`) and the arguments (`@*ARGS`), stripped to 100 characters (to account for utf8 multi-codepoint chars, and the md5 implementation limit of 128 codepoints).

# USE CASE
Why would you ever need such a simplistic and strange module? `Script::Hash` was designed for use in a module that requires a temporary directory that is keyed to the specific invocation of that module's binary. Basically, when one runs: `raku script.raku option1 option2` it creates a directory and uses it to store temporary files, based on the `$script-hash` variable provided by this module. Then, whenever `raku script.raku option1 option2` is run again (same arguments) the same temporary folder is used. When one calls `raku script.raku option3 option4 option5` on the other hand, then the `$script-hash` variable differs, and a new temporary directory is created. Not sure it can be used in any more cases than this… If you have any, let me know!

# AUTHOR
Mark Collins <tera_1225@hotmail.com>

# COPYRIGHT AND LICENSE
Copyright 2020 Mark Collins

This work is free. You can redistribute it and/or modify it under the terms of the Do What The Fuck You Want To Public License, Version 2, as published by Sam Hocevar. See the COPYING file for more details.