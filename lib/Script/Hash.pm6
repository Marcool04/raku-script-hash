use Digest;
unit module Script::Hash:ver<0.0.1>;

our $script-hash is export = md5(
    # comb(100)[0] is to only use first 100 code-points ; md5 is limited to 128...
    ($*PROGRAM-NAME~@*ARGS.join).encode('utf8-c8').decode('utf8').comb(100)[0]
  ).map({.base(16)}).join;


=begin pod

=head1 NAME

Script::Hash - A module to export a md5 hash of program name and arguments

=head1 SYNOPSIS

=begin code :lang<perl6>

use Script::Hash;
say "$script-hash" # output, example: 5067EEFF4999C7B5B4F593DD3521B9

=end code

=head1 DESCRIPTION

Script::Hash exports a single varibale, $script-hash, that can be used to access
an md5 hash of the program name ($*PROGRAM-NAME) and the arguments (@*ARGS),
stripped to 100 characters (to account for utf8 multi-codepoint chars, and the
md5 implementation limit of 128 codepoints).

=head1 USE CASE

Why would you ever need such a simplistic and strange module? Script::Hash was
designed for use in a module that requires a temporary directory that is keyed to
the specific invocation of that module's binary. Basically, when one runs:
C<raku script.raku option1 option2> it creates a directory and uses it to store
temporary files, based on the C<$script-hash> variable provided by this module.
Then, whenever C<raku script.raku option1 option2> is run again (same arguments)
the same temporary folder is used.
When one calls C<raku script.raku option3 option4 option5> on the other hand, then
the C<$script-hash> variable differs, and a new temporary directory is created.
Not sure it can be used in any more cases than this… If you have any, let me know!


=head1 AUTHOR

Mark Collins <tera_1225@hotmail.com>

=head1 COPYRIGHT AND LICENSE

Copyright 2020 Mark Collins

This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.
=end pod
